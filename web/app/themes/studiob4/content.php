<?php
/**
 * @package _s
 */
?>

<div class="row article-list">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row" data-equalizer>
			<div class="small-12 large-6 columns" data-equalizer-watch>
				<?php the_post_thumbnail('post-image'); ?>
			</div>
			<div class="small-12 large-6 columns content" data-equalizer-watch>
				<header class="entry-header">
					<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
				</header><!-- .entry-header -->
			
				<?php if ( is_search() ) : // Only display Excerpts for Search ?>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
				<?php else : ?>
				<div class="entry-content">
					<?php the_excerpt(); ?>
					<?php
						wp_link_pages( array(
							'before' => '<div class="page-links">' . __( 'Pages:', '_s' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
			</div>
		</div>
		
		<?php endif; ?>
	</article><!-- #post-## -->
</div>
