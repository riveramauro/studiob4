<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
<script>
// for bootstrapping page level code
var studiob4 = {
    "isFrontPage": <?= (is_front_page() ? 'true' : 'false'); ?>,
    "isPage":<?= (is_page() ? 'true' : 'false'); ?>,
    "templateURI": '<?= get_template_directory_uri(); ?>',
};
</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

	<div class="menu contain-to-grid sticky">
		<nav class="top-bar" data-topbar>
			<ul class="title-area">
			    <li class="name">
			    	<a href="<?php bloginfo( 'url' ); ?>"><h1 class="studiob4 show-for-small-only">Studio<span>B4</span></h1></a>
			    	<a href="<?php bloginfo( 'url' ); ?>"><div class="logo hide-for-small"></div><div class="sb4 hide-for-small"></div></a>
			    </li>
			     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
			</ul>
			<section class="top-bar-section">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'right') ); ?>
			</section>
		</nav>
	</div>
	
	<?php if ( is_front_page() ) {
		echo '<div id="content" class="site-content">';
	} else {
		echo '<div id="content" class="site-content	">';
	} ?>