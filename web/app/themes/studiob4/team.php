<?php
// Template Name: Team Page
?>
<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="row">
					<header class="entry-header small-12 columns">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->
				</div>

				<div class="entry-content">
					
					<div class="team">
						<div class="intro row">
							<div class="small-12 columns">
								<h3>We're a team that is not afraid to break the mold.</h3>
								<p class="hide-for-small">Some sort of introduction to the StudioB4 team. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque dolorem laudantium minima obcaecati hic quia ad dolorum unde odio, sed vel exercitationem adipisci, repellendus earum autem ipsum, in ducimus commodi!</p>
							</div>
						</div>
						<div class="row">
							<div class="profile small-12 medium-6 columns">
								<div class="bio-pic">
									<img alt="" src="<?php bloginfo( 'template_url' ); ?>/img/profile/lacguapa.jpg" />
								</div>
								<div class="bio">
									<h3 class="name">Lis Mery Ramirez</h3>
									<div class="title">Founder + CEO</div>
									<p class="hide-for-small">Lis Mery, “la C.Guapa,” is an art and music explorer who, after graduating from Rutgers University, set on a quest to share great stories. She is a poet and spoken word artist passionate about using technology and social media to communicate and bring people together.  Over the last 8 years, she has been hosting “Ultrason Latino” on WRSU FM, and is the current director for the Perth Amboy Gallery Center for the Arts - overseeing the development of city-wide creative initiatives.  She may have a slight obsession with strategic planning and showcasing talent, but that’s what makes her the mastermind behind StudioB4.</p>
								</div>
							</div>
							<div class="profile small-12 medium-6 columns">
								<div class="bio-pic">
									<img alt="" src="<?php bloginfo( 'template_url' ); ?>/img/profile/mauro.jpg" />
								</div>
								<div class="bio">
									<h3 class="name">Mauricio Rivera</h3>
									<div class="title">Creative Director</div>
									<p class="hide-for-small">Mauro is a fùtbol fiend, wannabe drummer and dad. First discovered design through “Obey,” Blek le Rat, and Banksy - making graphic design his way of life. Mauro has over 8 years of experience in the advertising and creative world working for a wide variety of clients.  He loves the craft of designing and finding visual solutions, developing new ideas, and thinking of new ways to change the world. He is the Creative Director of the world famous StudioB4.</p>
								</div>
							</div>
						</div>
						<!-- <div class="row">
							<div class="profile small-12 medium-6 columns">
								<div class="bio-pic">
									<img alt="" src="<?php bloginfo( 'template_url' ); ?>/img/profile/mauro.jpg" />
								</div>
								<div class="bio">
									<h3 class="name">Mauricio Rivera</h3>
									<div class="title">Creative Director</div>
									<p class="hide-for-small">Mauro is a fùtbol fiend, wannabe drummer and dad. First discovered design through “Obey,” Blek le Rat, and Banksy - making graphic design his way of life. Mauro has over 8 years of experience in the advertising and creative world working for a wide variety of clients.  He loves the craft of designing and finding visual solutions, developing new ideas, and thinking of new ways to change the world. He is the Creative Director of the world famous StudioB4.</p>
								</div>
							</div>
							<div class="profile small-12 medium-6 columns">
								<div class="bio-pic">
									<img alt="" src="http://placehold.it/800x500/0eafff/ffffff.png" />
								</div>
								<div class="bio">
									<h3 class="name">Sergio S. Camargo</h3>
									<div class="title">Audio-Visual Director</div>
									<p class="hide-for-small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, quibusdam vero, totam dolorum officiis perspiciatis! Dolorum atque veritatis quasi. Reiciendis recusandae, perferendis praesentium quas iure inventore, ad delectus animi ratione!</p>
								</div>
							</div>
						</div>
					</div> -->

				</div><!-- .entry-content -->
				<footer class="entry-footer">
					<?php edit_post_link( __( 'Edit', '_s' ), '<span class="edit-link">', '</span>' ); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

			

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>