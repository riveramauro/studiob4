$(document).foundation({
	orbit: {
		animation: 'slide',
		timer_speed: 8000,
		pause_on_hover: true,
		resume_on_mouseout: true,
		slide_number: false
	}
});
$(document).ready(function() {
	$('.home').backstretch( studiob4.templateURI + '/img/background.jpg');
	if($('body').hasClass('home')){
		$('div.menu').removeClass('sticky');
	}
	$('h1.single-title').fitText(1.5);
});