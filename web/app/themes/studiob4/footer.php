<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _s
 */
?>

	</div><!-- #content -->
</div><!-- #page -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="site-info">
		StudioB4 © <?php echo date("Y") ?> | <a href="https://www.facebook.com/instudiob4">fb.com/studiob4</a> | <a href="http://twitter.com">@studiob4</a>
	</div><!-- .site-info -->
</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
